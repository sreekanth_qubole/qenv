package config

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

// Config will contain the configuration for the whole deployment
type Config struct {
	v           *viper.Viper
	Environment string
	Region      string

	// VPC Settings
	VpcCidr                        string
	VpcEnableDNSSupport            bool
	VpcEnableDNSHostnames          bool
	VpcEnableClassicLink           bool
	VpcEnableClassicLinkDNSSupport bool

	// Private Subnet 1
	VpcEnablePvtSubnet1 bool
	VpcPvtSubnet1Cidr   string
	VpcPvtSubnet1Az     string

	// Private Subnet 2
	VpcEnablePvtSubnet2 bool
	VpcPvtSubnet2Cidr   string
	VpcPvtSubnet2Az     string

	// Public Subnet 1
	VpcEnablePubSubnet1 bool
	VpcPubSubnet1Cidr   string
	VpcPubSubnet1Az     string

	// Public Subnet 2
	VpcEnablePubSubnet2 bool
	VpcPubSubnet2Cidr   string
	VpcPubSubnet2Az     string

	// EIP and ENI
	VpcEnableEIP bool
}

// New returns an instance of Config filled with default values
func New() *Config {
	c := &Config{v: viper.New()}
	globalSettings := map[string]string{
		"Environment": "qib-test",
		"aws_region":  "us-east-1",
	}
	vpcSettings := map[string]interface{}{
		"vpc_cidr":                       "10.0.0.0/16",
		"enable_dns_support":             false,
		"enable_dns_hostnames":           false,
		"enable_classiclink":             true,
		"enable_classiclink_dns_support": false,

		//Private Subnet 1
		"enable_private_subnet_1": true,
		"private_subnet1_cidr":    "10.0.0.0/17",
		"private_subnet1_az":      "us-east-1a",

		//Private Subnet 2
		"enable_private_subnet_2": true,
		"private_subnet2_cidr":    "10.0.128.0/18",
		"private_subnet2_az":      "us-east-1d",

		//Public Subnet 1
		"enable_public_subnet_1": true,
		"public_subnet1_cidr":    "10.0.192.0/22",
		"public_subnet1_az":      "us-east-1a",

		//Public Subnet 2
		"enable_public_subnet_2": true,
		"public_subnet2_cidr":    "10.0.196.0/22",
		"public_subnet2_az":      "us-east-1a",
	}
	c.v.SetDefault("global", globalSettings)
	c.v.SetDefault("vpc", vpcSettings)
	return c
}

// GenerateFile will generate a sample configuration file in the same directory that should be edited and re-used for the deployment
func (c *Config) GenerateFile() error {
	err := c.v.WriteConfigAs("./deploy.toml")
	return err
}

// Parse parses the configuration file and populates fields in Config struct.
func (c *Config) Parse() error {
	c.v.SetConfigName("deploy")
	c.v.SetConfigType("toml")
	c.v.AddConfigPath(".")
	err := c.v.ReadInConfig()
	if err != nil {
		return err
	}
	err = c.populateFields()
	return err
}

func (c *Config) populateFields() error {
	global := c.v.GetStringMapString("global")
	for k, v := range global {
		if strings.ToLower(k) == "environment" {
			c.Environment = v
		}
		if strings.ToLower(k) == "aws_region" {
			c.Region = v
		}
	}

	vpc := c.v.GetStringMap("vpc")
	for k, v := range vpc {
		if strings.ToLower(k) == "vpc_cidr" {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", "vpc_cidr")
			}
			c.VpcCidr = val
		}

		if strings.ToLower(k) == "enable_dns_support" {
			val, ok := v.(bool)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", "enable_dns_support")
			}
			c.VpcEnableDNSSupport = val
		}

		if strings.ToLower(k) == "enable_dns_hostnames" {
			val, ok := v.(bool)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", "enable_dns_hostnames")
			}
			c.VpcEnableDNSHostnames = val
		}

		if strings.ToLower(k) == "enable_classiclink" {
			val, ok := v.(bool)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", "enable_classiclink")
			}
			c.VpcEnableClassicLink = val
		}

		if strings.ToLower(k) == "enable_classiclink_dns_support" {
			val, ok := v.(bool)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", "enable_classiclink_dns_support")
			}
			c.VpcEnableClassicLinkDNSSupport = val
		}

		/* Private Subnet 1 */
		settingName := "enable_private_subnet_1"
		if strings.ToLower(k) == settingName {
			val, ok := v.(bool)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcEnablePvtSubnet1 = val
		}

		settingName = "private_subnet1_cidr"
		if strings.ToLower(k) == settingName {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcPvtSubnet1Cidr = val
		}

		settingName = "private_subnet1_az"
		if strings.ToLower(k) == settingName {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcPvtSubnet1Az = val
		}

		/* Private Subnet 1 */
		settingName = "enable_private_subnet_2"
		if strings.ToLower(k) == settingName {
			val, ok := v.(bool)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcEnablePvtSubnet2 = val
		}

		settingName = "private_subnet2_cidr"
		if strings.ToLower(k) == settingName {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcPvtSubnet1Cidr = val
		}

		settingName = "private_subnet1_az"
		if strings.ToLower(k) == settingName {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcPvtSubnet1Az = val
		}

		/* Public Subnet 1 */
		settingName = "enable_public_subnet_1"
		if strings.ToLower(k) == settingName {
			val, ok := v.(bool)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcEnablePubSubnet1 = val
		}

		settingName = "public_subnet1_cidr"
		if strings.ToLower(k) == settingName {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcPubSubnet1Cidr = val
		}

		settingName = "public_subnet1_az"
		if strings.ToLower(k) == settingName {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcPubSubnet1Az = val
		}

		/* Public Subnet 2 */
		settingName = "enable_public_subnet_2"
		if strings.ToLower(k) == settingName {
			val, ok := v.(bool)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcEnablePubSubnet2 = val
		}

		settingName = "public_subnet2_cidr"
		if strings.ToLower(k) == settingName {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcPubSubnet2Cidr = val
		}

		settingName = "public_subnet2_az"
		if strings.ToLower(k) == settingName {
			val, ok := v.(string)
			if !ok {
				return fmt.Errorf("Invalid value for `vpc` setting %s", settingName)
			}
			c.VpcPubSubnet2Az = val
		}
	}

	return nil
}
