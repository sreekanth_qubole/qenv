provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "qenv" {
  cidr_block                     = "10.0.0.0/16"
  instance_tenancy               = "default"
  enable_dns_support             = false
  enable_dns_hostnames           = true
  enable_classiclink             = true
  enable_classiclink_dns_support = false
  tags = {
    Environment = "qenv"
    Name        = "qenv"
    Project     = "ops"
    Tier        = "vpc"
  }
}


resource "aws_subnet" "pub_az_1" {
  vpc_id                  = aws_vpc.qenv.id
  cidr_block              = "10.0.192.0/22"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = false
  tags = {
    Name        = "qenv-envname-us-east-1a-pub"
    Environment = "qenv"
  }
}

resource "aws_subnet" "pub_az_2" {
  vpc_id                  = aws_vpc.qenv.id
  cidr_block              = "10.0.196.0/22"
  availability_zone       = "us-east-1d"
  map_public_ip_on_launch = false
  tags = {
    Name        = "qenv-envname-us-east-1d-pub"
    Environment = "qenv"
  }
}

resource "aws_subnet" "pvt_az_1" {
  vpc_id                  = aws_vpc.qenv.id
  cidr_block              = "10.0.0.0/17"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = false
  tags = {
    Name        = "qenv-envname-us-east-1a-pvt"
    Environment = "qenv"
  }
}

resource "aws_subnet" "pvt_az_2" {
  vpc_id                  = aws_vpc.qenv.id
  cidr_block              = "10.0.128.0/18"
  map_public_ip_on_launch = false
  availability_zone       = "us-east-1d"
  tags = {
    Name        = "qenv-envname-us-east-1d-pvt"
    Environment = "qenv"
  }
}

// ENI
resource "aws_network_interface" "qenv" {
  subnet_id = aws_subnet.pub_az_1.id
  tags = {
    Name        = "qenv-pub-az1"
    Environment = "qenv"
  }
}

// EIP
resource "aws_eip" "qenv" {
  vpc               = true
  network_interface = aws_network_interface.qenv.id
  tags = {
    Name        = "qenv-pub-az1"
    Environment = "qenv"
  }
}

// NAT Gateway
resource "aws_nat_gateway" "qenv" {
  allocation_id = aws_eip.qenv.id
  subnet_id     = aws_subnet.pub_az_1.id
  tags = {
    Name        = "qenv-pub-az1"
    Environment = "qenv"
  }
}

// Internet Gateway
resource "aws_internet_gateway" "qenv" {
  vpc_id = aws_vpc.qenv.id
  tags = {
    Environment = "qenv"
    Name        = "qenv"
    Project     = "ops"
    Tier        = "vpc"
  }
}

// Route table that will be associated with all the private subnets
resource "aws_route_table" "qenv-pvt" {
  vpc_id = aws_vpc.qenv.id
  route {
    cidr_block     = "0.0.0.0"
    nat_gateway_id = aws_nat_gateway.qenv.id
  }
  // Add this route: Destination: 10.36.0.0/20 - target: pcx-abc1234 (the CIDR here refers to the CIDR of vpc-xyz0987 where the chef server is present)
}

resource "aws_route_table_association" "qenv-pvt-1" {
  subnet_id      = aws_subnet.pvt_az_1.id
  route_table_id = aws_route_table.qenv-pvt.id
}

resource "aws_route_table_association" "qenv-pvt-2" {
  subnet_id      = aws_subnet.pvt_az_2.id
  route_table_id = aws_route_table.qenv-pvt.id
}

// Route table that will be associated with all the public subnets
resource "aws_route_table" "qenv-pub" {
  vpc_id = aws_vpc.qenv.id
  route {
    cidr_block     = "0.0.0.0"
    nat_gateway_id = aws_internet_gateway.qenv.id
  }
  // Add this route: Destination: 10.36.0.0/20 - target: pcx-abc1234 (the CIDR here refers to the CIDR of vpc-xyz0987 where the chef server is present)
}

resource "aws_route_table_association" "qenv-pub-1" {
  subnet_id      = aws_subnet.pub_az_1.id
  route_table_id = aws_route_table.qenv-pvt.id
}

resource "aws_route_table_association" "qenv-pub-2" {
  subnet_id      = aws_subnet.pub_az_2.id
  route_table_id = aws_route_table.qenv-pvt.id
}
