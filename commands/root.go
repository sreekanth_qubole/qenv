package commands

import "github.com/spf13/cobra"

// RootCmd will be the root to which subcommands will be attached.
var RootCmd = &cobra.Command{
	Use:   "qenv <options>",
	Short: "Create Qubole environments",
}
