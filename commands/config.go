package commands

import (
	"fmt"
	"log"
	"qenv/config"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(cmdConfig)
	cmdConfig.AddCommand(configGen)
	cmdConfig.AddCommand(configValidate)
}

var cmdConfig = &cobra.Command{
	Use:   "config <option>",
	Short: "Provide qenv configuration management commands",
}

var configGen = &cobra.Command{
	Use:   "generate",
	Short: "Generate the default configuration file for qenv in current directory",
	Run:   generateConfig,
}

func generateConfig(cmd *cobra.Command, args []string) {
	cfg := config.New()
	err := cfg.GenerateFile()
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("Configuration file generated")
}

var configValidate = &cobra.Command{
	Use:   "validate",
	Short: "Parse and validate the configuration file",
	Run:   validateConfig,
}

func validateConfig(cmd *cobra.Command, args []string) {
	cfg := config.New()
	err := cfg.Parse()
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("Success! The configuration is valid.")
}
